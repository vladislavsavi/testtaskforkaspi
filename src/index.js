import 'normalize.css';
import 'bootstrap/scss/bootstrap.scss'
import './styles/bootstrap_all.scss';

import {eventInitialization} from "./scripts/product__card-body__button.js"
import {Cart} from "./scripts/cart";

window.onload = function () {
    window.cart = new Cart();
    console.log(cart);
    eventInitialization();
    basket.addEventListener('click', cart.togleCart);
    document.querySelector('.close-modal').addEventListener('click', cart.togleCart);
    let header = document.querySelector('.sticky');
    window.onscroll = function() {
        var scrolled = window.pageYOffset || document.documentElement.scrollTop;
        console.log(scrolled);
        if(scrolled > 66 ) {
            header.classList.add("sticky_fixed");
        } else {
            header.classList.remove("sticky_fixed");
        }
    }
};