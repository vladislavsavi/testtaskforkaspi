export function eventInitialization() {
    let arrButtons = document.querySelectorAll('.product__card-body__button');
    arrButtons.forEach((item) => {
        let price = item.getAttribute('price');
        let img = item.getAttribute('img');
        let name = item.getAttribute('name');
        let index = item.getAttribute('index');
        item.addEventListener('click', addToСart.bind(null, price, img, name, index));
    })
}

function addToСart(price, img, name, index , event) {
    event.currentTarget.disabled = true;
    let purchase = {
        price,
        img,
        name,
        index
    };
    window.cart.setCart = purchase;
}