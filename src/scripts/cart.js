export class Cart {
    constructor() {
        this.state = this.getCart;
        this.totalPrice = 0
    }

    get getCart() {
        //не успел реализовать сохранение в localStorage
        let storageCart = localStorage.getItem('cart');
        if (!storageCart) {
            return [];
        } else {
            return JSON.parse(storageCart);
        }
    }

    set setCart(purchase) {
        this.state.push(purchase);
        this.recountTotalPrice();
        this.distributeData();
    }

    recountTotalPrice() {
        this.totalPrice = 0;
        this.state.forEach((item) => {
            this.totalPrice += parseInt(item.price);
        });
    }

    removePurchase(index) {
        console.log(index);
        this.state = this.state.filter((item) => {
            return item.index != index;
        });
        this.recountTotalPrice();
        this.distributeData();
    }

    distributeData() {
        cart_sum.innerHTML = this.totalPrice;
        // JSX =D
        // createElement слишком долго
        let JSX = "";
        this.state.forEach((item) => {
            JSX += `<div class="alert purchase">
                <img class="purchase__img" src="${item.img}">
                <span class="purchase__name">${item.name}</span>
                <span class="purchase__price">${item.price}</span>
                <button type="button" index="${item.index}" class="close purchase__remove" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                </button>
             </div>`
        });
        modal_body.innerHTML = JSX;
        let btnArr = document.querySelectorAll('.purchase__remove');
        btnArr.forEach((item) => {
            item.addEventListener("click", this.removePurchase.bind(this, item.getAttribute('index')));
        });
    }

    togleCart(e) {
        modal.classList.toggle('show');
        if (modal.classList.contains('show')) {
            document.body.style.overflow = "hidden";
        } else {
            document.body.style.overflow = "scroll";
        }
    }
}